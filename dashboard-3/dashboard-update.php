<?php
/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of Dashboard.
 *
 * Dashboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Dashboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Dashboard. If not, see <http://www.gnu.org/licenses/>.
 */

if (updatePars() != 0)
{
    return 1;
}

if (updatePodcast() != 0)
{
    return 1;
}



function updatePars()
{
    clearstatcache();

    if (!(file_exists("./temp/") === true))
    {
        // Leading zero for file permission flags: octal.
        $success = @mkdir("./temp/", 0755, false);

        if ($success === false)
        {
            echo "Can't create directory \"./temp/\".\n";
            return 1;
        }
    }

    clearstatcache();

    if (file_exists("./temp/repository/") == true)
    {
        // Leading zero for file permission flags: octal.
        if (deletePathRecursively("./temp/repository/") != 0)
        {
            return 1;
        }
    }

    clearstatcache();

    $success = @mkdir("./temp/repository/", 0755, false);

    if ($success === false)
    {
        echo "Can't create directory \"./temp/repository/\".\n";
        return 1;
    }

    clearstatcache();

    $paths = array(array("./temp/repository/PeeragogicalActionReviews-master/anticipation-paper-progress/", "./top-left.xhtml"),
                   array("./temp/repository/PeeragogicalActionReviews-master/peeragogy-progress/", "./bottom-left.xhtml"),
                   array("./temp/repository/PeeragogicalActionReviews-master/par-repository-progress/", "./bottom-right.xhtml"));

    foreach ($paths as $path)
    {
        if (file_exists($path[1]) === true)
        {
            if (@unlink($path[1]) === true)
            {
                clearstatcache();
            }
            else
            {
                echo "Can't delete earlier target file \"".$path[1]."\".\n";
                return 1;
            }
        }
    }

    $repository = @file_get_contents("https://github.com/Peeragogy/PeeragogicalActionReviews/archive/master.zip",
                                     false,
                                     null, // HTTP options
                                     0,
                                     100 * 1024 * 1024);

    if ($repository === false)
    {
        echo "Can't retrieve \"https://github.com/Peeragogy/PeeragogicalActionReviews/archive/master.zip\".\n";
        return 1;
    }

    $byteCount = @file_put_contents("./temp/repository.zip", $repository);

    if ($byteCount === false)
    {
        echo "Can't save to \"./temp/repository.zip\".\n";
        return 1;
    }

    $zip = new ZipArchive();

    // , ZipArchive::RDONLY for second parameter $flags supported >= PHP 7.4.3.
    if (!($zip->open("./temp/repository.zip") === true))
    {
        echo "Can't open Zip archive \"./temp/repository.zip\".\n";
        return 1;
    }

    if (!($zip->extractTo("./temp/repository/") === true))
    {
        echo "Can't extract Zip archive \"./temp/repository/\".\n";
        $zip->close();
        return 1;
    }

    $zip->close();

    foreach ($paths as $path)
    {
        $files = @scandir($path[0], SCANDIR_SORT_DESCENDING);

        if (is_array($files) != true)
        {
            echo "Can't get the file list for \"".$path[0]."\".\n";
            return 1;
        }

        if (file_put_contents($path[1], "<ul>", FILE_APPEND | LOCK_EX) === false)
        {
            echo "Can't write to file \"".$path[1]."\".\n";
            return 1;
        }

        for ($i = 0, $max = count($files); $i < $max; $i++)
        {
            if (is_file($path[0].$files[$i]) == false)
            {
                continue;
            }

            if (handleFile($path[0].$files[$i], $path[1]) !== 0)
            {
                return 1;
            }
        }

        if (file_put_contents($path[1], "</ul>", FILE_APPEND | LOCK_EX) === false)
        {
            echo "Can't write to file \"".$path[1]."\".\n";
            return 1;
        }
    }

    return 0;
}

function handleFile($sourcePath, $destinationPath)
{
    $xml = simplexml_load_file($sourcePath);

    if ($xml === false)
    {
        echo "Can't load XML file \"".$sourcePath."\".\n";
        return 1;
    }

    if ($xml->getName() != "peeragogical-action-review")
    {
        echo "XML file \"".$sourcePath."\" with root element name \"".$xml->getName()."\", expected \"peeragogical-action-review\" instead.\n";
        return 1;
    }

    if ($xml->attributes()->title === null)
    {
        echo $sourcePath.": NO TITLE!\n";
        return 0;
    }

    echo $sourcePath.": ".$xml->attributes()->title."\n";

    if (file_put_contents($destinationPath, "<li>".htmlspecialchars($xml->attributes()->title, ENT_XHTML | ENT_QUOTES, "UTF-8")."</li>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$destinationPath."\".\n";
        return 1;
    }

    return 0;
}

function deletePathRecursively($path)
{
    clearstatcache();

    if (file_exists($path) != true)
    {
        return 0;
    }

    if (is_dir($path) == true)
    {
        $children = @scandir($path, SCANDIR_SORT_ASCENDING);

        if (is_array($children) != true)
        {
            echo "Can't get file list for \"".$path."\".\n";
            return 1;
        }

        for ($i = 0, $max = count($children); $i < $max; $i++)
        {
            if ($children[$i] == "." ||
                $children[$i] == "..")
            {
                continue;
            }

            $result = deletePathRecursively($path."/".$children[$i]);

            if ($result != 0)
            {
                return $result;
            }
        }

        if (rmdir($path) === true)
        {
            clearstatcache();
            return 0;
        }
        else
        {
            echo "Can't delete directory \"".$path."\".\n";
            return 1;
        }
    }
    else if (is_file($path) == true)
    {
        if (@unlink($path) === true)
        {
            clearstatcache();
            return 0;
        }
        else
        {
            echo "Can't delete file \"".$path."\".\n";
            return 1;
        }
    }
    else
    {
        echo "Unknown type at path \"".$path."\".\n";
        return 1;
    }
}



$entry = null;

function updatePodcast()
{
    clearstatcache();

    $path = "./top-right.xhtml";

    if (file_exists($path) === true)
    {
        if (@unlink($path) === true)
        {
            clearstatcache();
        }
        else
        {
            echo "Can't delete earlier target file \"".$path."\".\n";
            return 1;
        }
    }


    $repository = @file_get_contents("https://www.youtube.com/feeds/videos.xml?playlist_id=PLG6fmEnfJR2yaWGiK0tSp8QSis4btdCzE",
                                     false,
                                     null, // HTTP options
                                     0,
                                     100 * 1024 * 1024);

    if ($repository === false)
    {
        echo "Can't retrieve \"https://www.youtube.com/feeds/videos.xml?playlist_id=PLG6fmEnfJR2yaWGiK0tSp8QSis4btdCzE\".\n";
        return 1;
    }

    /** @todo This could also use my StAX parser implementation, which I would need to port to PHP from C++. */
    $sax = xml_parser_create();

    if ($sax === false)
    {
        echo "Error creating the SAX parser object.\n";
        return 1;
    }

    if (xml_parser_set_option($sax, XML_OPTION_CASE_FOLDING, false) !== true)
    {
        xml_parser_free($sax);
        echo "Error setting SAX parser option.\n";
        return 1;
    }

    if (xml_set_element_handler($sax, "StartElementHandler", "EndElementHandler") !== true)
    {
        xml_parser_free($sax);
        echo "Error registering event handler function.\n";
        return 1;
    }

    if (xml_set_character_data_handler($sax, "CharacterDataHandler") !== true)
    {
        xml_parser_free($sax);
        echo "Error registering event handler function.\n";
        return 1;
    }

    if (file_put_contents($path, "<ul>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$path."\".\n";
        return 1;
    }


    try
    {
        if (xml_parse($sax, $repository, true) !== 1)
        {
            xml_parser_free($sax);
            echo "Error during SAX parsing.\n";
            return 1;
        }
    }
    catch (Exception $ex)
    {
        xml_parser_free($sax);
        echo "Error during SAX parsing: ".$ex->getMessage()."\n";
        return 1;
    }

    xml_parser_free($sax);

    if (file_put_contents($path, "</ul>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$path."\".\n";
        return 1;
    }

    return 0;
}

function StartElementHandler($parser, $name, $attribs)
{
    global $entry;

    $path = "./top-right.xhtml";

    if ($name == "entry")
    {
        $entry = array();
    }
    else if ($name == "yt:videoId")
    {
        if (is_array($entry) == true)
        {
            $entry["in-yt:videoId"] = true;
            $entry["yt:videoId"] = "";
        }
    }
    else if ($name == "title")
    {
        if (is_array($entry) == true)
        {
            $entry["in-title"] = true;
            $entry["title"] = "";
        }
    }
}

function EndElementHandler($parser, $name)
{
    global $entry;

    $path = "./top-right.xhtml";

    if ($name == "yt:videoId")
    {
        if (is_array($entry) == true)
        {
            $entry["in-yt:videoId"] = false;
        }
    }
    else if ($name == "title")
    {
        if (is_array($entry) == true)
        {
            $entry["in-title"] = false;
        }
    }
    else if ($name === "entry")
    {
        if (is_array($entry) == true)
        {
            if (isset($entry["title"]) != true)
            {
                echo "entry without title.\n";
                return 1;
            }

            if (isset($entry["yt:videoId"]) != true)
            {
                echo "entry without yt:videoId.\n";
                return 1;
            }

            if (file_put_contents($path, "<li><a href=\"https://www.youtube.com/watch?v=".htmlspecialchars($entry["yt:videoId"], ENT_XHTML | ENT_QUOTES, "UTF-8")."\">".htmlspecialchars($entry["title"], ENT_XHTML, "UTF-8")."</a></li>", FILE_APPEND | LOCK_EX) === false)
            {
                echo "Can't write to file \"".$path."\".\n";
                return 1;
            }

            $entry = null;
        }

        $entry = null;
    }
}

function CharacterDataHandler($parser, $data)
{
    global $entry;

    if (is_array($entry) == true)
    {
        if (isset($entry["in-title"]) == true)
        {
            if ($entry["in-title"] == true)
            {
                if (is_string($entry["title"]) != true)
                {
                    $entry["title"] = "";
                }

                $entry["title"] .= $data;
            }
        }
        else if (isset($entry["in-yt:videoId"]) == true)
        {
            if ($entry["in-yt:videoId"] == true)
            {
                if (is_string($entry["yt:videoId"]) != true)
                {
                    $entry["yt:videoId"] = "";
                }

                $entry["yt:videoId"] .= $data;
            }
        }
    }
}



?>
