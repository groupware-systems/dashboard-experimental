#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of Dashboard.
#
# Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with Dashboard. If not, see <http://www.gnu.org/licenses/>.

php dashboard.php > dashboard.xhtml
