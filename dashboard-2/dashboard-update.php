<?php
/* Copyright (C) 2020-2021 Stephan Kreutzer
 *
 * This file is part of Dashboard.
 *
 * Dashboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Dashboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Dashboard. If not, see <http://www.gnu.org/licenses/>.
 */

clearstatcache();

if (!(file_exists("./temp/") === true))
{
    // Leading zero for file permission flags: octal.
    $success = @mkdir("./temp/", 0755, false);

    if ($success === false)
    {
        echo "Can't create directory \"./temp/\".\n";
        return 1;
    }
}

clearstatcache();

if (file_exists("./temp/repository/") == true)
{
    // Leading zero for file permission flags: octal.
    if (deletePathRecursively("./temp/repository/") != 0)
    {
        return 1;
    }
}

clearstatcache();

$success = @mkdir("./temp/repository/", 0755, false);

if ($success === false)
{
    echo "Can't create directory \"./temp/repository/\".\n";
    return 1;
}

clearstatcache();

$paths = array(array("./temp/repository/PeeragogicalActionReviews-master/anticipation-paper-progress/", "./top-left.xhtml"),
               array("./temp/repository/PeeragogicalActionReviews-master/par-repository-progress/", "./top-right.xhtml"),
               array("./temp/repository/PeeragogicalActionReviews-master/peeragogy-progress/", "./bottom-left.xhtml"),
               array("./temp/repository/PeeragogicalActionReviews-master/wrap-workinggroup-progress/", "./bottom-right.xhtml"));

foreach ($paths as $path)
{
    if (file_exists($path[1]) === true)
    {
        if (@unlink($path[1]) === true)
        {
            clearstatcache();
        }
        else
        {
            echo "Can't delete earlier target file \"".$path[1]."\".";
            return 1;
        }
    }
}

$repository = @file_get_contents("https://github.com/Peeragogy/PeeragogicalActionReviews/archive/master.zip",
                                 false,
                                 null, // HTTP options
                                 0,
                                 100 * 1024 * 1024);

if ($repository === false)
{
    echo "Can't retrieve \"https://github.com/Peeragogy/PeeragogicalActionReviews/archive/master.zip\".\n";
    return 1;
}

$byteCount = @file_put_contents("./temp/repository.zip", $repository);

if ($byteCount === false)
{
    echo "Can't save to \"./temp/repository.zip\".\n";
    return 1;
}

$zip = new ZipArchive();

// , ZipArchive::RDONLY for second parameter $flags supported >= PHP 7.4.3.
if (!($zip->open("./temp/repository.zip") === true))
{
    echo "Can't open Zip archive \"./temp/repository.zip\".\n";
    return 1;
}

if (!($zip->extractTo("./temp/repository/") === true))
{
    echo "Can't extract Zip archive \"./temp/repository/\".\n";
    $zip->close();
    return 1;
}

$zip->close();

foreach ($paths as $path)
{
    $files = @scandir($path[0], SCANDIR_SORT_DESCENDING);

    if (is_array($files) != true)
    {
        echo "Can't get the file list for \"".$path[0]."\".\n";
        return 1;
    }

    if (file_put_contents($path[1], "<ul>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$path[1]."\".\n";
        return 1;
    }

    for ($i = 0, $max = count($files); $i < $max; $i++)
    {
        if (is_file($path[0].$files[$i]) == false)
        {
            continue;
        }

        if (handleFile($path[0].$files[$i], $path[1]) !== 0)
        {
            return 1;
        }
    }

    if (file_put_contents($path[1], "</ul>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$path[1]."\".\n";
        return 1;
    }
}


function handleFile($sourcePath, $destinationPath)
{
    $xml = simplexml_load_file($sourcePath);

    if ($xml === false)
    {
        echo "Can't load XML file \"".$sourcePath."\".\n";
        return 1;
    }

    if ($xml->getName() != "peeragogical-action-review")
    {
        echo "XML file \"".$sourcePath."\" with root element name \"".$xml->getName()."\", expected \"peeragogical-action-review\" instead.\n";
        return 1;
    }

    if ($xml->attributes()->title === null)
    {
        echo $sourcePath.": NO TITLE!\n";
        return 0;
    }

    echo $sourcePath.": ".$xml->attributes()->title."\n";

    if (file_put_contents($destinationPath, "<li>".htmlspecialchars($xml->attributes()->title, ENT_XHTML | ENT_QUOTES, "UTF-8")."</li>", FILE_APPEND | LOCK_EX) === false)
    {
        echo "Can't write to file \"".$destinationPath."\".\n";
        return 1;
    }

    return 0;
}

function deletePathRecursively($path)
{
    clearstatcache();

    if (file_exists($path) != true)
    {
        return 0;
    }

    if (is_dir($path) == true)
    {
        $children = @scandir($path, SCANDIR_SORT_ASCENDING);

        if (is_array($children) != true)
        {
            echo "Can't get file list for \"".$path."\".\n";
            return 1;
        }

        for ($i = 0, $max = count($children); $i < $max; $i++)
        {
            if ($children[$i] == "." ||
                $children[$i] == "..")
            {
                continue;
            }

            $result = deletePathRecursively($path."/".$children[$i]);

            if ($result != 0)
            {
                return $result;
            }
        }

        if (rmdir($path) === true)
        {
            clearstatcache();
            return 0;
        }
        else
        {
            echo "Can't delete directory \"".$path."\".\n";
            return 1;
        }
    }
    else if (is_file($path) == true)
    {
        if (@unlink($path) === true)
        {
            clearstatcache();
            return 0;
        }
        else
        {
            echo "Can't delete file \"".$path."\".\n";
            return 1;
        }
    }
    else
    {
        echo "Unknown type at path \"".$path."\".\n";
        return 1;
    }
}




?>
