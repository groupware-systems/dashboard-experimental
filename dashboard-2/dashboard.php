<?php
/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of Dashboard.
 *
 * Dashboard is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Dashboard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Dashboard. If not, see <http://www.gnu.org/licenses/>.
 */

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".
     "<!DOCTYPE html\n".
     "    PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n".
     "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n".
     "<html version=\"-//W3C//DTD XHTML 1.1//EN\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\" lang=\"en\">\n".
     "  <head>\n".
     "    <meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>\n".
     "    <!-- This file was created by dashboard.php of Dashboard, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/groupware-systems/dashboard/ and https://groupware-systems.org). -->\n".
     "    <!--\n".
     "    Copyright (C) 2020 Stephan Kreutzer\n".
     "    \n".
     "    This file is part of Dashboard.\n".
     "    \n".
     "    Dashboard is free software: you can redistribute it and/or modify\n".
     "    it under the terms of the GNU Affero General Public License version 3 or any later version,\n".
     "    as published by the Free Software Foundation.\n".
     "    \n".
     "    Dashboard is distributed in the hope that it will be useful,\n".
     "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n".
     "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n".
     "    GNU Affero General Public License 3 for more details.\n".
     "    \n".
     "    You should have received a copy of the GNU Affero General Public License 3\n".
     "    along with Dashboard. If not, see <http://www.gnu.org/licenses/>.\n".
     "    \n".
     "    The data in <div id=\"panel-top-left\"/>, <div id=\"panel-top-right\"/>,\n".
     "    <div id=\"panel-bottom-left\"/> and <div id=\"panel-bottom-right\"/> is not\n".
     "    part of this program, it's user data which is only processed. A different\n".
     "    license may apply.\n".
     "    -->\n".
     "    <title>Dashboard</title>\n".
     "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n".
     "    <style type=\"text/css\">\n".
     "      body\n".
     "      {\n".
     "          font-family: sans-serif;\n".
     "      }\n".
     "\n".
     "      #panel-top-left\n".
     "      {\n".
     "          width: 48%;\n".
     "          height: 48%;\n".
     "          position: absolute;\n".
     "          left: 1%;\n".
     "          top: 1%;\n".
     "          overflow: auto;\n".
     "          border: 1px solid #000000;\n".
     "      }\n".
     "\n".
     "      #panel-top-right\n".
     "      {\n".
     "          width: 48%;\n".
     "          height: 48%;\n".
     "          position: absolute;\n".
     "          left: 51%;\n".
     "          top: 1%;\n".
     "          overflow: auto;\n".
     "          border: 1px solid #000000;\n".
     "      }\n".
     "\n".
     "      #panel-bottom-left\n".
     "      {\n".
     "          width: 48%;\n".
     "          height: 48%;\n".
     "          position: absolute;\n".
     "          left: 1%;\n".
     "          top: 51%;\n".
     "          overflow: auto;\n".
     "          border: 1px solid #000000;\n".
     "      }\n".
     "\n".
     "      #panel-bottom-right\n".
     "      {\n".
     "          width: 48%;\n".
     "          height: 48%;\n".
     "          position: absolute;\n".
     "          left: 51%;\n".
     "          top: 51%;\n".
     "          overflow: auto;\n".
     "          border: 1px solid #000000;\n".
     "      }\n".
     "    </style>\n".
     "  </head>\n".
     "  <body>\n".
     "    <div>\n".
     "      <!-- The data contained within the <div/> elements below is not part of this program, it's user data and might be provided under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. -->\n".
     "      <div id=\"panel-top-left\">\n";

@include "./top-left.xhtml";

echo "      </div>\n".
     "      <div id=\"panel-top-right\">\n";

@include "./top-right.xhtml";

echo "      </div>\n".
     "      <div id=\"panel-bottom-left\">\n";

@include "./bottom-left.xhtml";

echo "      </div>\n".
     "      <div id=\"panel-bottom-right\">\n";

@include "./bottom-right.xhtml";

echo "      </div>\n".
     "    </div>\n".
     "  </body>\n".
     "</html>\n";

?>
